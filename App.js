import "react-native-gesture-handler";
import React from "react";

import AlbumList from "./components/Album/AlbumList";
import PhotoList from "./components/Album/Photo/PhotoList";
import { NavigationContainer } from "@react-navigation/native";
import { StatusBar, useColorScheme } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import Header from "./components/UI/header";

const Stack = createStackNavigator();

export const App = () => (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        name="albumList"
        component={AlbumList}
        options={{ title: "Albums" }}
      />
      <Stack.Screen
        name="photoList"
        component={PhotoList}
        options={{ title: "Photos" }}
      />
    </Stack.Navigator>
  </NavigationContainer>
);
