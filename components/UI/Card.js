import React from "react";
import { View } from "react-native";

const Card = (props) => {
  return <View style={styles.containerStyle}>{props.children}</View>;
};

const styles = {
  containerStyle: {
    borderWidth: 1,
    borderRadius: "0.5em",
    borderColor: "#ddd",
    backgroundColor: "white",
    boxShadow: "0em 0.5em 0.5em rgba(0,0,0,0.1)",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    margin: "1em",
    padding: "0.5em",
  },
};

export default Card;
