import React from "react";
import { Text, TouchableOpacity } from "react-native";

const Button = ({ onPress, children }) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Text style={textStyle}>{children}</Text>
    </TouchableOpacity>
  );
};

const Badge = ({ onPress, children }) => {
  const { badgeStyle, textBadgeStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={badgeStyle}>
      <Text style={textBadgeStyle}>{children}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: "center",
    color: "white",
    fontSize: 16,
    fontWeight: "600",
    paddingTop: 10,
    paddingBottom: 10,
  },
  textBadgeStyle: {
    alignSelf: "center",
    color: "white",
    fontSize: 16,
    fontWeight: "600",
  },
  buttonStyle: {
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "#007aff",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#007aff",
    marginLeft: 5,
    marginRight: 5,
  },
  badgeStyle: {
    flex: 1,
    padding: "0px",
    marginTop: "0.5em",
    marginBottom: "0.5em",
    width: "30px",
    height: "50px",
    borderRadius: "50px",
    lineHeight: "50px",
    alignSelf: "stretch",
    backgroundColor: "#007aff",
    //borderRadius: "100%",
    borderWidth: 1,
    borderColor: "#007aff",
  },
};

export { Button, Badge };
