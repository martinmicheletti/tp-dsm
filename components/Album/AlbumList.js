import React, { Component, useEffect, useState } from "react";
import { ScrollView, Text, View, FlatList } from "react-native";
import axios from "axios";
import AlbumDetail from "./AlbumDetail";

const AlbumList = (props) => {
  const [photoSet, setPhotoSet] = React.useState(null);

  React.useEffect(() => {
    let url =
      "https://api.flickr.com/services/rest/?method=flickr.photosets.getList&api_key=6e8a597cb502b7b95dbd46a46e25db8d&user_id=137290658%40N08&format=json&nojsoncallback=1";
    axios
      .get(url)
      .then((response) => setPhotoSet(response.data.photosets.photoset));
  }, []);

  React.useEffect(() => {
    console.log(photoSet);
  }, [photoSet]);

  const renderAlbum = ({ item }) => (
    <AlbumDetail
      navigation={props.navigation}
      key={item.id}
      title={item.title._content}
      desc={item.description._content}
      photoCount={item.photos}
      albumId={item.id}
    />
  );

  return !photoSet ? (
    <Text>Loading...</Text>
  ) : (
    <View style={{ flex: 1 }}>
      <View style={{ margin: "1em" }}>
        <Text style={{ fontWeight: 600, fontSize: 16 }}>TP - DSM - 2022</Text>
        <Text style={{ fontWeight: 500, fontSize: 16 }}>70120</Text>
        <Text>Martín Micheletti</Text>
      </View>
      <FlatList
        data={photoSet}
        renderItem={renderAlbum}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
};

// class AlbumList extends Component {
//   state = { photoset: null };

//   componentWillMount() {
//     axios
//       .get(
//         "https://api.flickr.com/services/rest/?method=flickr.photosets.getList&api_key=6e8a597cb502b7b95dbd46a46e25db8d&user_id=137290658%40N08&format=json&nojsoncallback=1"
//       )
//       .then((response) =>
//         this.setState({ photoset: response.data.photosets.photoset })
//       );
//   }

//   renderAlbums() {
//     return this.state.photoset.map((album) => (
//       <AlbumDetail
//         navigation={this.props.navigation}
//         key={album.id}
//         title={album.title._content}
//         albumId={album.id}
//       />
//     ));
//   }

//   render() {
//     console.log(this.state);

//     if (!this.state.photoset) {
//       return <Text>Loading...</Text>;
//     }

//     return (
//       <View style={{ flex: 1 }}>
//         <ScrollView>{this.renderAlbums()}</ScrollView>
//       </View>
//     );
//   }
// }

export default AlbumList;
