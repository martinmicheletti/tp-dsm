import React from "react";
import { Text, View, Image, Linking } from "react-native";
import Card from "../UI/Card";
import CardSection from "../UI/CardSection";
import { Button, Badge } from "../UI/Button";

const AlbumDetail = ({ navigation, title, desc, photoCount, albumId }) => {
  const {
    headerContentStyle,
    thumbnailStyle,
    thumbnailContainerStyle,
    imageStyle,
    headerTextStyle,
  } = styles;

  return (
    <Card>
      <CardSection>
        <View style={headerContentStyle}>
          <Badge>{photoCount}</Badge>
          <Text style={headerTextStyle}>{title}</Text>
          <Text>{desc}</Text>
          {/* <View style={thumbnailContainerStyle}>
            <Image style={thumbnailStyle} source={{ uri: imageUrl }} />
          </View> */}
        </View>
      </CardSection>
      <CardSection>
        <Button
          onPress={() =>
            navigation.navigate("photoList", {
              albumId: albumId,
              albumName: title,
            })
          }
        >
          Ver
        </Button>
      </CardSection>
    </Card>
  );
};

const styles = {
  headerContentStyle: {
    flexDirection: "column",
    justifyContent: "space-around",
    paddingTop: "0.5em",
    paddingBottom: "0.5em",
  },
  headerTextStyle: {
    fontSize: 18,
    fontWeight: 500,
  },
  thumbnailStyle: {
    height: 50,
    width: 50,
  },
  thumbnailContainerStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null,
  },
  thumbnailStyle: {
    height: 50,
    width: 50,
  },
  thumbnailContainerStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
  },
};

export default AlbumDetail;
